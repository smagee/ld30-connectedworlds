#include <vector>

#include <SFML/Graphics.hpp>

#include "Player.h"
#include "Planet.h"

const float windowWidth = 800.0f;
const float windowHeight = 600.0f;

bool isDead = true;
bool showMenu = true;

void pollMenuWindow(sf::RenderWindow &window);
void pollGameWindow(sf::RenderWindow &window);

void checkInput(Player &player);
void checkCollision(sf::RenderWindow &window, Player &player, Planet &earth);
void display(sf::RenderWindow &window, sf::View &view, sf::CircleShape &player, sf::CircleShape &earth);

void displayMenu(sf::RenderWindow &window);

int main()
{
	sf::RenderWindow gameWindow(sf::VideoMode(windowWidth, windowHeight), "Ludum Dare 30 - Connected Worlds");
	gameWindow.setFramerateLimit(60);

	Player player("Player", gameWindow.getDefaultView());
	player.setPosition(windowWidth, windowHeight);
	player.resetView();

	Planet earth(500.0f);

	while (gameWindow.isOpen() && showMenu == true)
	{
		if (showMenu == true)
		{
			pollMenuWindow(gameWindow);
			displayMenu(gameWindow);
		}

		while (isDead == false)
		{
			pollGameWindow(gameWindow);

			checkInput(player);

			checkCollision(gameWindow, player, earth);

			display(gameWindow, player.getView(), player.getPlayer(), earth.getPlanet());
		}
	}

	return EXIT_SUCCESS;
}

void pollMenuWindow(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			isDead = false;
			showMenu = false;
		}
	}
}

void pollGameWindow(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			isDead = true;
			window.close();
		}
	}
}

void checkInput(Player &player)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		player.getPlayer().move(-10.0f*player.getSpeed(), 0.0f);
		player.getView().move(-10.0f*player.getSpeed(), 0.0f);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		player.getPlayer().move(10.0f*player.getSpeed(), 0.0f);
		player.getView().move(10.0f*player.getSpeed(), 0.0f);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		player.getPlayer().move(0.0f, -10.0f*player.getSpeed());
		player.getView().move(0.0f, -10.0f*player.getSpeed());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		player.getPlayer().move(0.0f, 10.0f*player.getSpeed());
		player.getView().move(0.0f, 10.0f*player.getSpeed());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		//player.increaseSize(player.getCurrentSize()*2);
	}
}

void checkCollision(sf::RenderWindow &window, Player &player, Planet &earth)
{
	sf::FloatRect playerBox = player.getPlayer().getGlobalBounds();
	sf::FloatRect earthBox = earth.getPlanet().getGlobalBounds();

	if (playerBox.intersects(earthBox) && player.getCurrentSize() < earth.getSize())
		player.getPlayer().setFillColor(sf::Color::Blue);
	else
		player.getPlayer().setFillColor(sf::Color::Green);
}

void display(sf::RenderWindow &window, sf::View &view, sf::CircleShape &player, sf::CircleShape &earth)
{
	window.clear();
	window.setView(view);
	window.draw(player);
	window.draw(earth);
	window.display();
}

void displayMenu(sf::RenderWindow &window)
{
	sf::Font font;
	if (!font.loadFromFile("assets/arial.ttf"))
	{

	}
	sf::Text title("Asteroid Absorption", font, 75);
	sf::Text instructionTitle("Instructions:\n", font, 50);
	instructionTitle.move(0, 100);
	sf::Text instructions("Collide with other asteroids to absorb them and grow bigger, you may\nonly absorb asteroids that are smaller than you, and will die if you try to\nabsorb anything bigger, your goal is to grow big enough to absorb the\nearth", font, 25);
	instructions.move(0, 150);
	sf::Text playText("Press 'Space' to start game.\n", font, 50);
	playText.move(0, 300);

	window.clear();
	window.draw(title);
	window.draw(instructions);
	window.draw(instructionTitle);
	window.draw(playText);
	window.display();
}
