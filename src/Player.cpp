#include "Player.h"

Player::Player(std::string name, sf::View view)
{
	name_ = name;
	score_ = 0;

	speed_ = 1.0f;

	currentSize_ = 10.0f;
	maxSize_ = 500.0f;

	setPlayer();

	view_ = view;
	view_.zoom(2.0f);
}

Player::~Player()
{
	//dtor
}

std::string Player::getName()
{
	return name_;
}

void Player::incrementScore()
{
	score_++;
}

void Player::decrementScore()
{
	score_--;
}

int Player::getScore()
{
	return score_;
}

void Player::absorb(Planet planet)
{
	increaseSize(planet.getSize());
}

void Player::increaseSpeed(float incSize)
{
	speed_ += incSize/2;
}

void Player::decreaseSpeed(float decSize)
{
	speed_ -= decSize/2;
}

float Player::getSpeed()
{
	return speed_;
}

void Player::increaseSize(float incSize)
{
	currentSize_ += incSize;
	increaseSpeed(currentSize_);
	player_.setRadius(currentSize_);
	increaseZoom(currentSize_);
}

void Player::decreaseSize(float decSize)
{
	currentSize_ -= decSize;
	decreaseSpeed(currentSize_);
	player_.setRadius(currentSize_);
	decreaseZoom(currentSize_);
}

float Player::getCurrentSize()
{
	return currentSize_;
}

void Player::increaseZoom(float incSize)
{
	view_.zoom(incSize);
}

void Player::decreaseZoom(float decSize)
{
	view_.zoom(decSize);
}

float Player::getMaxSize()
{
	return maxSize_;
}

void Player::setPosition(float x, float y)
{
	player_.setPosition(x, y);
}

sf::CircleShape& Player::getPlayer()
{
	return player_;
}

sf::View& Player::getView()
{
	return view_;
}

void Player::resetView()
{
	view_.setCenter(player_.getPosition());
}

void Player::setPlayer()
{
	player_.setRadius(currentSize_);
	player_.setOrigin(player_.getRadius(), player_.getRadius());
	player_.setFillColor(sf::Color::Green);
	//if (!texture_.loadFromFile("assets/player.png"))
	//{
		//crash
	//}
	//sprite_.setTexture(texture_)
}
