#include "Planet.h"

Planet::Planet(float radius)
{
	size_ = radius;
	isVisible = false;
	setPlanet();
}

Planet::~Planet()
{
	//dtor
}

void Planet::setSize(float radius)
{
	planet_.setRadius(radius);
}

float Planet::getSize()
{
	return size_;
}

void Planet::setPosition(float x, float y)
{
	planet_.setPosition(x, y);
}

sf::CircleShape& Planet::getPlanet()
{
	return planet_;
}

void Planet::setPlanet()
{
	setSize(size_);
	planet_.setOrigin(planet_.getRadius(), planet_.getRadius());
	planet_.setFillColor(sf::Color::Red);
}
