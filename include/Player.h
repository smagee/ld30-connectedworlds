#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include <SFML/Graphics.hpp>

#include "Planet.h"

class Player
{
	public:
		Player(std::string name, sf::View view);
		virtual ~Player();

		std::string getName();

		void incrementScore();
		void decrementScore();
		int getScore();

		void absorb(Planet planet);

		void increaseSpeed(float incSize);
		void decreaseSpeed(float decSize);
		float getSpeed();

		void increaseSize(float incSize);
		void decreaseSize(float decSize);
		float getCurrentSize();

		void increaseZoom(float incSize);
		void decreaseZoom(float decSize);

		float getMaxSize();

		void setPosition(float x, float y);

		sf::CircleShape& getPlayer();

		sf::View& getView();
		void resetView();
	protected:
	private:
		std::string name_;
		int score_;

		float speed_;

		float currentSize_;
		float maxSize_;

		sf::CircleShape player_;
		void setPlayer();

		sf::View view_;

		//sf::Texture texture_;
		//sf::Sprite sprite_;
};

#endif // PLAYER_H
