#ifndef PLANET_H
#define PLANET_H

#include <SFML/Graphics.hpp>

class Planet
{
	public:
		Planet(float radius);
		virtual ~Planet();

		void setSize(float radius);
		float getSize();

		void setPosition(float x, float y);

		sf::CircleShape& getPlanet();
	protected:
	private:
		float size_;
		bool isVisible;

		sf::CircleShape planet_;
		void setPlanet();
};

#endif // PLANET_H
